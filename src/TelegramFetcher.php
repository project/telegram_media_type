<?php

namespace Drupal\telegram_media_type;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Template\Attribute;
use GuzzleHttp\ClientInterface;

/**
 * Sets embedded data for TelegramEmbedFormatter.
 */
class TelegramFetcher {

  /**
   * Stores logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $loggerChannel;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Tracks when an error has occurred when interacting with the API.
   *
   * @var bool
   */
  protected $apiErrorEncountered = FALSE;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructor for TelegramFetcher.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger factory.
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_channel_factory, ClientInterface $client, LanguageManagerInterface $language_manager, ConfigFactoryInterface $configFactory, CacheBackendInterface $cache) {
    $this->loggerChannel = $logger_channel_factory->get('telegram_media_type');
    $this->httpClient = $client;
    $this->languageManager = $language_manager;
    $this->cache = $cache;
  }

  /**
   * Returns embed HTML code for telegram media.
   *
   * @param string $resource_url
   *   Telegram post URL.
   *
   * @return array
   *   Embedded response data.
   */
  public function getOembedData($resource_url) {
    $postLink = '';
    $content_url_regex = '/https:\/\/t\.me\/(?<id>.+)/i';
    $embed_regex = '/data-telegram-post\=\"(?<id>[^\"]+)\"/i';
    $matches = [];
    if (preg_match($content_url_regex, $resource_url, $matches) || preg_match($embed_regex, $resource_url, $matches)) {
      $postLink = $matches['id'];
    }
    $urlToDisplay = 'https://t.me/' . $postLink;

    $scriptAttributes = new Attribute();
    $scriptAttributes['data-telegram-post'] = [$postLink];
    $scriptAttributes['data-width'] = ['100%'];
    $scriptAttributes['data-is-telegram'] = ['true'];

    $oembedResponse['html'] = [
      '#theme' => 'telegram_media_type',
      '#url' => $urlToDisplay,
      '#script_attributes' => $scriptAttributes,
    ];

    $oembedResponse['author_name'] = 'Telegram';
    $oembedResponse['width'] = '500';
    $oembedResponse['height'] = 'auto';
    $oembedResponse['url'] = $urlToDisplay;

    return $oembedResponse;
  }

}
