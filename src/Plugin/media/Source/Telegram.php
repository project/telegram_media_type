<?php

namespace Drupal\telegram_media_type\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\telegram_media_type\TelegramFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Telegram entity media source.
 *
 * @MediaSource(
 *   id = "telegram",
 *   label = @Translation("Telegram"),
 *   description = @Translation("Embed telegram posts."),
 *   allowed_field_types = {"string_long"},
 *   default_thumbnail_filename = "telegram.jpg",
 *   forms = {
 *     "media_library_add" = "\Drupal\telegram_media_type\Form\TelegramMediaLibraryAddForm",
 *   }
 * )
 */
class Telegram extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * Telegram Fetcher.
   *
   * @var \Drupal\telegram_media_type\TelegramFetcher
   */
  protected $telegramFetcher;

  /**
   * Constructs Telegram media source.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\telegram_media_type\TelegramFetcher $telegram_fetcher
   *   The Telegram fetcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, TelegramFetcher $telegram_fetcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->telegramFetcher = $telegram_fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('telegram_media_type.telegram_fetcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'author_name' => $this->t('Author Name'),
      'width' => $this->t('Width'),
      'height' => $this->t('Height'),
      'url' => $this->t('URL'),
      'html' => $this->t('HTML'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $content_url = $this->getTelegramUrl($media);
    if ($content_url === FALSE) {
      return FALSE;
    }

    $data = $this->telegramFetcher->getOembedData($content_url);
    if ($data === FALSE) {
      return FALSE;
    }

    switch ($attribute_name) {
      case 'author_name':
        return $data['author_name'];

      case 'width':
        return $data['width'];

      case 'height':
        return $data['height'];

      case 'url':
        return $this->getTelegramUrl($media);

      case 'html':
        return $data['html'];

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * Runs preg_match on embed code/URL.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media object.
   *
   * @return string|false
   *   The Telegram url or FALSE if there is no field or it contains invalid
   *   data.
   */
  protected function getTelegramUrl(MediaInterface $media) {
    if (isset($this->configuration['source_field'])) {
      $source_field = $this->configuration['source_field'];
      if ($media->hasField($source_field)) {
        $property_name = $media->{$source_field}->first()->mainPropertyName();
        $embed = $media->{$source_field}->{$property_name};

        return $embed;
      }
    }

    return FALSE;
  }

  /**
   * Extract a Telegram content URL from a string.
   *
   * Typically users will enter an iframe embed code that Telegram provides, so
   * which needs to be parsed to extract the actual post URL.
   *
   * Users may also enter the actual content URL - in which case we just return
   * the value if it matches our expected format.
   *
   * @param string $data
   *   The string that contains the Telegram post URL.
   *
   * @return string|bool
   *   The post URL, or FALSE if one cannot be found.
   */
  public static function parseTelegramEmbedField($data) {
    $data = trim($data);

    // Ideally we would verify that the content URL matches an exact pattern,
    // but Telegram has a ton of different ways posts/notes/videos/etc URLs can
    // be formatted, so it's not practical to try and validate them. Instead,
    // just validate that the content URL is from the Telegram domain.
    $content_url_regex = '/https:\/\/t\.me\/(?<id>.+)/i';
    $embed_regex = '/data-telegram-post\=\"(?<id>[^\"]+)\"/i';

    if (preg_match($content_url_regex, $data) || preg_match($embed_regex, $data)) {
      return $data;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'telegram_embed',
      'label' => 'visually_hidden',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return ['TelegramEmbedCode' => []];
  }

}
