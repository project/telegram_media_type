<?php

namespace Drupal\telegram_media_type\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\telegram_media_type\Plugin\media\Source\Telegram;

/**
 * Plugin implementation of the 'telegram_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "telegram_embed",
 *   label = @Translation("Telegram embed"),
 *   field_types = {
 *     "link", "string", "string_long"
 *   }
 * )
 */
class TelegramEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getEntity();

    $element = [];
    if (($source = $media->getSource()) && $source instanceof Telegram) {
      foreach ($items as $delta => $item) {
        $element[$delta] = [
          '#cache' => [
            'contexts' => Cache::mergeContexts(['languages'], $media->getCacheContexts()),
            'tags' => $media->getCacheTags(),
            'max-age' => $media->getCacheMaxAge(),
          ],
        ];

        $element[$delta] += $source->getMetadata($media, 'html');
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() === 'media';
  }

}
