<?php

namespace Drupal\telegram_media_type\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Check if a value is a valid Telegram embed code or post URL.
 *
 * @Constraint(
 *   id = "TelegramEmbedCode",
 *   label = @Translation("Telegram embed code", context = "Validation"),
 *   type = { "link", "string", "string_long" }
 * )
 */
class TelegramEmbedCodeConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'Not valid Telegram post URL/embed code.';

}
