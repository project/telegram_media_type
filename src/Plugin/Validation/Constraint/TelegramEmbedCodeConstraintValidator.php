<?php

namespace Drupal\telegram_media_type\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\telegram_media_type\Plugin\media\Source\Telegram;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the TelegramEmbedCode constraint.
 */
class TelegramEmbedCodeConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $data = NULL;
    if (is_string($value)) {
      $data = $value;
    }
    elseif ($value instanceof FieldItemInterface) {
      $class = get_class($value);
      $property = $class::mainPropertyName();
      if ($property) {
        $data = $value->$property;
      }
    }

    if ($data) {
      $post_url = Telegram::parseTelegramEmbedField($value);
      if ($post_url === FALSE) {
        $this->context->addViolation($constraint->message);
      }
    }

  }

}
