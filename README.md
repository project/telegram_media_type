# Telegram Media Type

Adds a media type for embedding Telegram posts and videos to your site.

## Table of contents

- Installation
- Configuration
- Usage
- Other info
- Maintainers


## Installation

Install as usual, see <https://www.drupal.org/docs/extending-drupal/installing-modules>
for further information.


## Configuration

Module required Drupal 8.8 or newer.

1. Enable module
2. create Telegram media type
- Go to /admin/structure/media/add
- set Name to Telegram
- set Description to Telegram posts.
- set Media source to Telegram
- click Save
3. set formatter for field Telegram to Telegram embed if it's not set already.
4. (optional) set form widgets and form fields at
   /admin/structure/media/manage/telegram/form-display.


## Usage

Create telegram media using entity forms (/media/add/telegram), or using Drupal
core's Media Library. Just paste the URL and the module generates the
embed code.


## Other info

The module's codebase was based on how Media Entity Twitter and Media Entity
Facebook was written.


## Maintainers

- Robert Kasza - [kaszarobert](https://www.drupal.org/u/kaszarobert)
